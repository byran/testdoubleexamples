#ifndef TESTDOUBLEEXAMPLES_ORDER_HPP
#define TESTDOUBLEEXAMPLES_ORDER_HPP

#include "removable_inventory.hpp"
#include <string>
#include <map>

class order
{
private:
    std::map<std::string, int> required_items;
    bool fulfilled;
public:
    order();
    void add_required_item(int quantity, const std::string& item);
    bool complete() const;

    void fulfill(removable_inventory & inventory);
};

#endif //TESTDOUBLEEXAMPLES_ORDER_HPP
