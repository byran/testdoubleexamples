#include "order.hpp"
#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
    class removable_inventory_stub : public removable_inventory
    {
    public:
        removable_inventory_stub() :
                itemsInStockReturn(true)
        {
        }

        bool itemsInStockReturn;
        virtual bool items_in_stock(int quantity, const std::string &item) const override
        {
            return itemsInStockReturn;
        }

        virtual void remove_items_from_inventory(int quantity, const std::string &item)
        {

        }
    };
}

TEST(Stub, Fulfilling_an_order_makes_it_complete)
{
    // Setup
    removable_inventory_stub inventory;

    order singleItemOrder;
    singleItemOrder.add_required_item(5, "Apples");

    // Exercise
    singleItemOrder.fulfill(inventory);

    // Verify
    ASSERT_TRUE(singleItemOrder.complete());

    // Teardown
}

TEST(Stub, When_items_are_out_of_stock_trying_to_fulfill_an_order_does_not_make_it_complete)
{
    // Setup
    removable_inventory_stub inventory;
    inventory.itemsInStockReturn = false;

    order singleItemOrder;
    singleItemOrder.add_required_item(5, "Apples");

    // Exercise
    singleItemOrder.fulfill(inventory);

    // Verify
    ASSERT_FALSE(singleItemOrder.complete());

    // Teardown
}