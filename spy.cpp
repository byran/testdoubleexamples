#include "order.hpp"
#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
    class removable_inventory_spy : public removable_inventory
    {
    public:
        removable_inventory_spy() :
                quantityStockChecked(0),
                quantityRemoved(0)
        {

        }

        mutable int quantityStockChecked;
        mutable std::string itemStockChecked;

        virtual bool items_in_stock(int quantity, const std::string &item) const override
        {
            quantityStockChecked = quantity;
            itemStockChecked = item;
            return true;
        }

        int quantityRemoved;
        std::string itemRemoved;

        virtual void remove_items_from_inventory(int quantity, const std::string &item) override
        {
            quantityRemoved = quantity;
            itemRemoved = item;
        }
    };
}

TEST(Spy, Fulfilling_a_single_item_order_removes_the_correct_number_of_items_from_the_inventory)
{
    // Setup
    removable_inventory_spy inventory;

    order singleItemOrder;
    singleItemOrder.add_required_item(5, "Plums");

    // Exercise
    singleItemOrder.fulfill(inventory);

    // Verify
    ASSERT_EQ(5, inventory.quantityStockChecked);
    ASSERT_EQ("Plums", inventory.itemStockChecked);

    ASSERT_EQ(5, inventory.quantityRemoved);
    ASSERT_EQ("Plums", inventory.itemRemoved);

    // Teardown
}
