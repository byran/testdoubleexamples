#ifndef TESTDOUBLEEXAMPLES_REMOVABLE_INVENTORY_HPP
#define TESTDOUBLEEXAMPLES_REMOVABLE_INVENTORY_HPP

#include <string>

class removable_inventory
{
public:
    virtual ~removable_inventory() { }

    virtual bool items_in_stock(int quantity, const std::string & item) const = 0;
    virtual void remove_items_from_inventory(int quantity, const std::string & item)= 0;
};

#endif //TESTDOUBLEEXAMPLES_REMOVABLE_INVENTORY_HPP
