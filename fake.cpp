#include "order.hpp"
#include <gtest/gtest.h>

#include <map>

using namespace ::testing;

namespace
{
    class removable_inventory_fake : public removable_inventory
    {
    public:
        std::map<std::string, int> items;

        virtual bool items_in_stock(int quantity, const std::string &item) const override
        {
            return items.count(item) != 0 && items.at(item) >= quantity;
        }

        virtual void remove_items_from_inventory(int quantity, const std::string &item) override
        {
            if (items.count(item) != 0)
            {
                items[item] -= quantity;
            }
        }
    };
}

TEST(Fake, When_only_some_of_the_order_items_are_in_stock_an_order_is_not_completed)
{
    // Setup
    removable_inventory_fake inventory;
    inventory.items["Apples"] = 10;

    order twoItemOrder;
    twoItemOrder.add_required_item(5, "Pears");
    twoItemOrder.add_required_item(5, "Apples");

    // Exercise
    twoItemOrder.fulfill(inventory);

    // Verify
    ASSERT_FALSE(twoItemOrder.complete());

    // Teardown
}