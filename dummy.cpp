#include "order.hpp"
#include <gtest/gtest.h>

#include <exception>

using namespace ::testing;

namespace
{
    class removable_inventory_dummy : public removable_inventory
    {
    public:
        virtual bool items_in_stock(int quantity, const std::string &item) const override
        {
            throw std::exception();
        }

        virtual void remove_items_from_inventory(int quantity, const std::string &item) override
        {
            throw std::exception();
        }
    };
}

TEST(Dummy, An_empty_order_cannot_be_completed)
{
    // Setup
    removable_inventory_dummy inventory;

    order emptyOrder;

    // Exercise
    emptyOrder.fulfill(inventory);

    // Verify
    ASSERT_FALSE(emptyOrder.complete());

    // Teardown
}