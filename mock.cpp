#include "order.hpp"
#include <gmock/gmock.h>

using namespace ::testing;

namespace
{
    class removable_inventory_mock : public removable_inventory
    {
    public:
        MOCK_CONST_METHOD2(items_in_stock, bool(int quantity, const std::string &item));
        MOCK_METHOD2(remove_items_from_inventory, void(int quantity, const std::string &item));
    };
}

TEST(Mock, When_fulfilling_an_order_no_items_are_removed_from_the_inventory_if_some_of_the_items_are_out_of_stock)
{
    // Setup
    removable_inventory_mock inventory;


    EXPECT_CALL(inventory, items_in_stock(5, "Apples"))
            .Times(1)
            .WillOnce(Return(true));

    EXPECT_CALL(inventory, items_in_stock(5, "Pears"))
            .Times(1)
            .WillOnce(Return(false));

    EXPECT_CALL(inventory, remove_items_from_inventory(_, _))
            .Times(0);

    order twoItemOrder;
    twoItemOrder.add_required_item(5, "Apples");
    twoItemOrder.add_required_item(5, "Pears");

    // Exercise
    twoItemOrder.fulfill(inventory);

    // Verify

    // Teardown
}