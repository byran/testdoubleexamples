#include "order.hpp"

order::order() :
fulfilled(false)
{
}

void order::add_required_item(int quantity, const std::string& item)
{
    required_items[item] += quantity;
}

bool order::complete() const
{
    return fulfilled;
}

void order::fulfill(removable_inventory &inventory)
{
    if (required_items.size() == 0)
        return;

    fulfilled = true;
    for (const auto &item : required_items)
    {
        if (!inventory.items_in_stock(item.second, item.first))
        {
            fulfilled = false;
        }
    }

    if(fulfilled)
    {
        for (const auto &item : required_items)
        {
            inventory.remove_items_from_inventory(item.second, item.first);
        }

    }
}
